#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# ESI Pressure Gauge startup cmd
# -----------------------------------------------------------------------------
require(streamdevice)
require(esipressuregauge)
require(iocstats)

# -----------------------------------------------------------------------------
# Environment variables
# -----------------------------------------------------------------------------
epicsEnvSet("IPADDR",   "192.168.1.254")
epicsEnvSet("IPPORT",   "4002")
epicsEnvSet("SYS",      "ESTIA-PFEIFFER-001")
epicsEnvSet("LOCATION", "$(SYS) $(IPADDR)")
epicsEnvSet("DEV",      "")
epicsEnvSet("PREFIX",   "$(SYS)")
epicsEnvSet("IOCNAME",  "$(PREFIX)")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(esipressuregauge_DIR)db")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# Statistics database
iocshLoad("$(iocstats_DIR)iocStats.iocsh", "IOCNAME=$(IOCNAME)")

# ESI Pressure Gauge
iocshLoad("$(esipressuregauge_DIR)esiPressureGauge.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")
